(function () {

  'use strict';

  angular.module('pipedrive-resellers', ['ui.bootstrap'])

  .controller('resellersController', ['$scope', '$log', '$http', '$timeout', 'filterFilter',
  	function($scope, $log, $http, $timeout, filterFilter) {

		$scope.submitButtonText = "Registrar";
		$scope.loading = false;
		$scope.urlError = false;
		
		function getDeals() {
	  		var poller = function() {
	    		$http.get('/_listdeals').
	      			success(function(data, status, headers, config) {
						if (status === 200){
							$scope.deals = data;
	          				return false;
	        			}
	      			}).
	      			error(function(error) {
	            		$log.log(error);
	          		});
	  		};
	  		poller();
		}

		getDeals();

    	$scope.sendDeal = function() {
	       	$scope.loading = true;
	        $scope.submitButtonText = "Registrando...";
	        $http.post('/senddeal', {"customer": $scope.input_customer, "contact": $scope.input_contact, 
	        	"title": $scope.input_title, "value": $scope.input_value, "description": $scope.input_description }).
	          success(function(results) {
	            $scope.input_customer = null;
	            $scope.input_contact = null;
	           	$scope.input_title = null;
	           	$scope.input_value = null;
	           	$scope.input_description = null;
	          	$scope.loading = false;
                $scope.submitButtonText = "Registrar";
	            $scope.urlError = false;
	            $timeout(getDeals, 1000);
	          }).
	          error(function(error) {
	          	$scope.loading = false;
                $scope.submitButtonText = "Registrar";
                $scope.urlError = error;
	            $log.log(error);
	          });
    	};

	}
  ]);

}());