# -*- coding: UTF-8 -*-
from flask import request
from flask import Response
from flask import Flask
from flask import render_template
from flask import make_response
from flask import abort, redirect, url_for, flash, get_flashed_messages, send_from_directory, jsonify, safe_join
from flaskext.auth import Auth, AuthUser, login_required, get_current_user_data
from flaskext.auth import logout as flask_logout
from flaskext.auth.auth import not_logged_in
from datetime import datetime
from pipedrive import PipedriveAPI
from pipedrive.models import Deal, Organization
from pipedrive.types import PipedriveModelType
import json
import os
import sys
import re
import mimetypes
import shutil

app = Flask('pipedrive-resellers')
app.config.update(
    DEBUG=True,
    TESTING=True,
    SECRET_KEY='XXXXXXXXXXXX',
    PRODUCTION_MODE=True,
    PIPEDRIVE_API="XXXXXXXXXXXXX",
    STAGE_WAITING=(7,),
    STAGE_REVIEWING=(8,),
    STAGE_APPROVED=(9,),
    PIPELINE_APPROVED=(4,),
    PIPELINE_DENIED=(5,),
    RESELLER_KEY="XXXXXXXXXXX",
)
auth = Auth(app, login_url_name="login")
users = {'test':'test'}

@app.before_request
def init_users():
    app.config.users = {}
    for user, password in users.items():
        admin = AuthUser(username=user)
        admin.set_and_encrypt_password(password)
        app.config.users[user] = admin

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        if username in app.config.users:
            # Authenticate and log in!
            if app.config.users[username].authenticate(request.form['password']):
                return redirect('/')
        flash(u"Usuário ou senha inválida", 'warning')
        return redirect('/login')
    else:
        return render_template('login.html')

@app.route("/logout")
def logout():
    flask_logout()
    return redirect('/')

@app.route("/")
@login_required()
def home():
    return render_template('home.html', logged_user=get_current_user_data().get('username', ''))

@app.route("/_listdeals")
def listdeals():
    if get_current_user_data() is None:
        return not_logged_in(None)

    api = PipedriveAPI(app.config['PIPEDRIVE_API'])
    #TODO: Filter proper deals based on auth
    org = api.organization.find("group_name")
    deals = api.organization.list_deals(org[0].id)

    ret = {}
    for deal in deals:
    	stage = 0
    	if deal.stage_id.id in app.config['STAGE_WAITING']:
    		stage = 1
    	elif deal.stage_id.id in app.config['STAGE_REVIEWING']:
    		stage = 2
    	elif deal.stage_id.id in app.config['STAGE_APPROVED']:
    		stage = 3
    	else:
			pipeline_id = api.stage.detail(deal.stage_id.id).pipeline_id.id
			if pipeline_id in app.config['PIPELINE_APPROVED']:
				stage = 3
			elif pipeline_id in app.config['PIPELINE_DENIED']:
				stage = 4

        ret[deal.id] = {
            'title': deal.title,
            'customer': deal.org_id.name,
            'value': unicode(deal.value),
            'add_time': deal.add_time.strftime("%d/%m/%Y %H:%M"),
            'stage': stage, 
            }

    return jsonify(ret)

@app.route("/senddeal", methods=['POST'])
def senddeal():
	if get_current_user_data() is None:
		return not_logged_in(None)
	data = json.loads(request.data.decode('utf-8'))
	print data
	#TODO: Add a new Deal
	needed_keys = ['customer', 'contact', 'title', 'value', 'description']
	for i in needed_keys:
		if not data.get(i, None):
			return u"Preencha todos os campos, por favor", 400

	api = PipedriveAPI(app.config['PIPEDRIVE_API'])
	customer = api.organization.find(data['customer'])

	try:
		customer_id = customer[0].id
	except (IndexError, AttributeError):
		return u"Cliente não encontrado", 400

	new_deal = Deal(raw_data=dict(org_id=customer_id, stage_id=app.config['STAGE_WAITING'][0], title=data['title'],
		value=data['value']))
	new_deal._fields[app.config['RESELLER_KEY']] = PipedriveModelType(Organization, required=False)
	setattr(new_deal, app.config['RESELLER_KEY'], 1)
	print new_deal.to_primitive()

	created = api.deal.create(new_deal)
	print created.id

	# Add Description as a Notes

	return u"Success"

if __name__ == '__main__':
    try:
        if sys.argv[1] == "-d":
            app.config["PRODUCTION_MODE"] = False
            app.run(host='0.0.0.0', port=8080, debug=True)
    except IndexError:
        app.run(host='0.0.0.0', port=80, debug=False)

